<!--

 // set the Dystopian Daily issue title.
 dd_issue_title = '2018.12.14';

 // set the Dystopian Daily issue _display_ title [i.e. the title show in the window title, etc.].
 dd_issue_display_title = '[Λ•] Dystopian Daily | ' + dd_issue_title;

 // set the 'progress bar type'.
 progress_bar_type = 'landing_page';

 // ---

 // initialise the 'progress increment time interval' (in milliseconds) variable.
 progress_bar_increment_interval = 500;

 // initialise the 'loaded images counter' variable.
 loaded_images_counter = [];
 loaded_images_counter['feature'] = 0;
 loaded_images_counter['dreams'] = 0;
 loaded_images_counter['reality'] = 0;

 // initialise the 'progress percentage complete' variable.
 //  progress_percentage = 0;
 progress_percentage = [];
 progress_percentage['feature'] = 0;
 progress_percentage['dreams'] = 0;
 progress_percentage['reality'] = 0;

 // initiate progress bar incrementation.
 loaded_images_index = 0;

 // set the loading order for each section.
 zine_sections = ['feature', 'dreams', 'reality',];


 // ---

function load_all_zine_images()
{
 for (section_counter = 0; section_counter < zine_sections.length; section_counter++)
 { load_images(zine_sections[section_counter]); }
}

// ---

function load_images(images_type)
{
  // initialise the 'image urls' list.
  dd_images = [];

  // if 'images_type' variable is not set,
  if (typeof images_type == 'undefined')
  {
   // default to 'feature'type
   images_type = 'feature';
  }
    
  switch (images_type)
  {
   case "feature":
      dd_images = modal_feature_image_urls.slice();
      zine_section_heading = "Feature";
   break;

   case "dreams":
      // retrieve all images for this section.
      pages_length = dreams_pages.length;
      for (i=0; i<pages_length; i++)
      { try { dd_images = dd_images.concat(dreams_pages[i]['gallery_image_urls']); }
	catch(err){} }
      // alert(images_type + '|' + dd_images.length);
      zine_section_heading = "Dreams";
   break;

   case "reality":
      // retrieve all images for this section.
      pages_length = reality_pages.length;
      for (i=0; i<pages_length; i++)
      { try { dd_images = dd_images.concat(reality_pages[i]['gallery_image_urls']); }
	catch(err){} }
      // alert(images_type + '|' + dd_images.length);
      zine_section_heading = "Reality";
   break;
  }

  get_images_for_section(zine_section_heading, dd_images, images_type);
}

function get_images_for_section(zine_section_heading, dd_images, images_type)
{
  window.status = "[Λ•] Loading " + zine_section_heading + "... this may take a while...";
  document.getElementById('landing_page_progress_section_container').innerHTML = zine_section_heading;

  // retrieve filenames of the images to be loaded.
  number_of_images_to_be_loaded = dd_images.length;

  // initialise the 'progress increment' variable.
  progress_increment = Math.round(100 / number_of_images_to_be_loaded);
    
  // ---

  // if the number of images loaded doesn't exceeded the total number of images,n
  if (loaded_images_counter[images_type] < number_of_images_to_be_loaded)
  {
   // load the next image.
   // rem: images are not only .png filetype. [2019.03.16].
   // this_png_filename = magazine_issue_images[loaded_images_index].png_filename;
   this_png_filename = dd_images[loaded_images_counter[images_type]];
   this_png_filename_location = get_png_file_location(images_type, this_png_filename);

   // test.
   // alert('[' + images_type + ']|[' + loaded_images_counter[images_type] + ']: this_png_filename_location: ' + this_png_filename_location);

   // create a new Image object.
   var img = new Image();

   // increment the 'percent complete' by the progress increment.
   progress_percentage[images_type] += progress_increment;
   if (progress_percentage[images_type] > 100){ progress_percentage[images_type] = 100; }

   // create an output 'percentage' variable for the Javascript loading bar 'width'
   //  and progress bar HTML display properties.
   progress_percentage_percent = progress_percentage[images_type] + "%";

   // when the image has loaded, increment the progress bar.
   img.onload = update_progress_bar(progress_percentage_percent, progress_percentage_percent, progress_bar_type, this_png_filename_location);

   // set the new Image object's src to this image filename.
   img.src = this_png_filename_location;

   // increment the 'number of images loaded' variable.
   loaded_images_index += 1;

   // increment the 'loaded images counter' variable.
   loaded_images_counter[images_type] += 1;

   // update window status.
   window.status = "[Λ•] Loading " + zine_section_heading + " | " + progress_percentage_percent + " complete...";

   // increment the image progress bar.
   setTimeout(function(){ get_images_for_section(zine_section_heading, dd_images, images_type); }, progress_bar_increment_interval);
  }
  else
  {
    // ---

    // if this is final image to load, set the progress bar to '100%'.
    // update the progress bar.
    update_progress_bar(100, '100%', progress_bar_type);

    // wait one second, then clear the 'cover logo' div background image.
    setTimeout(function(){
	document.getElementById('cover_logo').style.backgroundImage = "none";
	document.getElementById('landing_page_progress_section_container').innerHTML = '';
    }, 1000);


      // if this is the final section to be loaded,
      final_zine_section_index = zine_sections.length - 1;

      // test.
      // alert(images_type + " | " +  zine_sections[final_zine_section_index]);
      
      if (images_type == zine_sections[final_zine_section_index])
      {
       // update window status.
       window.status = "[Λ•] Starpunk Zine is loaded. Are you ready? :) Preparing display...";

       // set the 'images loaded' cookie.
       // cookie_value = '1';
       // days_to_live = 1;
       // createCookie(cookie_name, cookie_value, days_to_live);

       if (progress_bar_type == 'landing_page')
       {
	  // hide the 'please wait sign.
	  document.getElementById('please_wait').style.display='none';

	  // display the 'please enter' sign.
	  document.getElementById('please_enter').style.display='block';
	  document.getElementById('please_enter').style.color='#FFF';

	  // highlight the 'Enter AltSciFi' text.
	  document.getElementById('cover_text').style.color='#FFF';

	  // open the main page.
	  // open_dd_page_interval = 500;
	  // setTimeout(open_peachtrees, open_dd_page_interval);

	  // display the password prompt.
	  open_dd_password_box_interval = 500;
	  setTimeout(show_password_box, open_dd_password_box_interval);
       }
      }
  }
}

 // ---

 function get_png_file_location(images_type, image_filename)
 {
     // image_filename_location = dd_issue_title + '/images/' + image_filename;
     // image_filename_location = 'images/' + images_type + '_images/' + image_filename;
     image_filename_location = image_filename;
     return image_filename_location;
 }


 function open_peachtrees()
 {
     // hide the 'Enter AltSciFi' landing page (i.e. the div).
     document.getElementById('landing_page').style.display = 'none';

     // display the Dystopian Daily issue page div.
     document.getElementById('page_container').style.display = 'block';

     window.status = dd_issue_display_title;
     window.document.title = dd_issue_display_title;
 }

 // ---

function update_progress_bar(progress_percentage_percent, progress_percentage_percent, progress_bar_type, this_png_filename_location)
 {
   // test.
   // alert('updating progress bar: ' + progress_percentage_percent + ' | progress_percentage_percent: ' + progress_percentage_percent)

   // update the progress bar div's width.
   document.getElementById('loading_bar').style.width = progress_percentage_percent;

   // display a thumbnail version of the image in the 'logo' background.
   document.getElementById('cover_logo').style.backgroundImage = "url(" + this_png_filename_location + ")";
     
   // update the progress bar div's display HTML.
   if (progress_bar_type == 'landing_page'){ loading_bar_text = progress_percentage_percent; }
   else { loading_bar_text = "[Λ•] " + progress_percentage_percent; }

   document.getElementById('loading_bar_text').innerHTML = loading_bar_text;
 }

// --- cookie functions.
//     credit: http://www.quirksmode.org/js/cookies.html

function createCookie(name,value,days)
{
 if (days)
 {
  var date = new Date();
  date.setTime(date.getTime()+(days*24*60*60*1000));
  var expires = "; expires="+date.toGMTString();
 }
 else var expires = "";
 document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name)
{
 var nameEQ = name + "=";
 var ca = document.cookie.split(';');
 for (var i=0;i < ca.length;i++)
 {
  var c = ca[i];
  while (c.charAt(0)==' ') c = c.substring(1,c.length);
  if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
 }
 return null;
}

function toggle_content_removal_policy_modal(toggle_state)
{
    if (toggle_state == 'open')
    { document.getElementById('content_removal_policy_modal').style.display='block'; }

    if (toggle_state == 'close')
    { document.getElementById('content_removal_policy_modal').style.display='none'; }
}

//-->

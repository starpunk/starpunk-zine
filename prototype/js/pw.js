<!---

function show_password_box()
{
 // set the 'password box' object.
 password_box_name = 'password_input_container';
 password_box_obj = document.getElementById(password_box_name);

 // set the 'password input' field object.
 password_input_field_name = 'password_input';
 password_input_field_obj = document.getElementById(password_input_field_name);

 // display the password box.
 password_box_obj.style.display='block';

 // focus the password input field.
 password_input_field_obj.focus();
}

function check_password()
{
 // correct_password = "--- encrypted password ---";
 password_json = JSON.parse('--- encrypted password json string ---');

 correct_password = decrypt_password(password_json);

 // set the 'password input' field object.
 password_input_field_name = 'password_input';
 password_input_field_obj = document.getElementById(password_input_field_name);

 // retrieve the password entered by the user.
 attempted_password = password_input_field_obj.value;

 // if the password entered by the user is correct,
 if (attempted_password == correct_password)
 {
  // outline empty the password box in green.
  password_input_field_obj.style.borderColor='#77FC42';

  // hide the 'enter' button.
  document.getElementById('enter_site_button').style.display='none';

  // open the main page.
  open_dd_password_box_interval = 500;
  setTimeout(function(){
	  open_peachtrees();
      },
      open_dd_password_box_interval);
 }
 else
 {
  // empty the password box and outline in red.
  password_input_field_obj.value ='';
  password_input_field_obj.style.borderColor='#CC0000';

  // change the password box outline to neutral
  // and focus the password field.
  open_dd_password_box_interval = 1000;
  setTimeout(function(){
	  password_input_field_obj.style.borderColor='#666';
	  password_input_field_obj.focus();
      },
      open_dd_password_box_interval);
 }
}

function  decrypt_password(encrypted)
{
 var key = CryptoJS.enc.Hex.parse(encrypted.key),
     iv = CryptoJS.enc.Hex.parse(encrypted.iv),
     cipher = CryptoJS.lib.CipherParams.create({
	     ciphertext: CryptoJS.enc.Base64.parse(encrypted.ciphertext)
	 }),
     result = CryptoJS.AES.decrypt(cipher, key, {iv: iv, mode: CryptoJS.mode.CFB});

 decrypted = result.toString(CryptoJS.enc.Utf8);

 return decrypted;
}

// credit: https://stackoverflow.com/a/11365682
document.getElementById('password_input').onkeypress = function(e)
{
 if (!e) e = window.event;
 var keyCode = e.keyCode || e.which;
 if (keyCode == '13')
 {
  // 'Enter' key pressed.

  // check the password.
  check_password();

  return false;
 }
}

 //-->
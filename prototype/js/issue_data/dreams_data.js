<!--

dreams_pages = new Array;
dreams_pages[0] = '';
// rem: the zeroeth index is left blank, so that the program can directly access
//      items using the page number [i.e. page 1, page 2, etc.] using the array index.


// data for the first 'dreams' section page.
dreams_pages[1] = '';

// ----------------------------------------------------------------------------
// data for the second 'dreams' section page.

// - 1. modal window items.
dreams_page_two_item_title_one = `Siichele`;
dreams_page_two_item_text_one = `
		    <p>
		      instagram.com/personone
		    <p>
    `;

dreams_page_two_item_title_two = `Person Two`;
dreams_page_two_item_text_two = `
		    <p>
		      instagram.com/persontwo
		    <p>
    `;

dreams_page_two_item_title_three = `Person Three`;
dreams_page_two_item_text_three = `
		    <p>
		      instagram.com/personthree
		    <p>
    `;

dreams_page_two_item_title_four = `Abby Lastname`;
dreams_page_two_item_text_four = `
		    <p>
		      instagram.com/abbylastname
		    <p>
    `;

dreams_page_two_item_title_five = `Person Five`;
dreams_page_two_item_text_five = `
		    <p>
		      instagram.com/personfive
		    <p>
    `;

dreams_page_two_item_title_six = `Person Six`;
dreams_page_two_item_text_six = `
		    <p>
		      instagram.com/personsix
		    <p>
    `;


// ---
// - 2. page items.

dreams_page_two_item_page_text_one = `
	      <div class="vertical_citation_text">
		<p>
		  <span class="social_symbol">
		    A
		  </span>
		  instagram.com/siichele
		</p>
		<p>
		  <span class="social_symbol">
		    U
		  </span>
		  twitter.com/siichele
		</p>
	      </div>
`;

dreams_pages[2] = {
    'gallery_image_urls': ['images/dreams_images/tumblr_pdsbasjQeF1xe2075o1_1280.jpg',
			   'images/dreams_images/tumblr_pdsnwr6qI81xe2075o1_1280.jpg',
			   'images/dreams_images/tumblr_pdsda99Zzg1xe2075o1_1280.jpg',
			   'images/dreams_images/tumblr_pduabjrps11xe2075o1_1280.jpg',
			   'images/dreams_images/tumblr_pdubpdlpeM1xe2075o1_1280.jpg',
			   'images/dreams_images/tumblr_peuwmfvQaC1xe2075o4_1280.jpg',
			  ],
    'modal_body_items': [{'title': dreams_page_two_item_title_one,
			  'text': dreams_page_two_item_text_one,
			 },
			 {'title': dreams_page_two_item_title_two,
			  'text': dreams_page_two_item_text_two,
			 },
			 {'title': dreams_page_two_item_title_three,
			  'text': dreams_page_two_item_text_three,
			 },
			 {'title': dreams_page_two_item_title_four,
			  'text': dreams_page_two_item_text_four,
			 },
			 {'title': dreams_page_two_item_title_five,
			  'text': dreams_page_two_item_text_five,
			 },
			 {'title': dreams_page_two_item_title_six,
			  'text': dreams_page_two_item_text_six,
			 },
			],

    'page_body_items': [{'title': dreams_page_two_item_title_one,
			  'text': dreams_page_two_item_page_text_one,
			 },
			 {'title': dreams_page_two_item_title_two,
			  'text': dreams_page_two_item_text_two,
			 },
			 {'title': dreams_page_two_item_title_three,
			  'text': dreams_page_two_item_text_three,
			 },
			 {'title': dreams_page_two_item_title_four,
			  'text': dreams_page_two_item_text_four,
			 },
			 {'title': dreams_page_two_item_title_five,
			  'text': dreams_page_two_item_text_five,
			 },
			 {'title': dreams_page_two_item_title_six,
			  'text': dreams_page_two_item_text_six,
			 },
			],

}


//-->

<!--

reality_pages = new Array;
reality_pages[0] = '';
// rem: the zeroeth index is left blank, so that the program can directly access
//      items using the page number [i.e. page 1, page 2, etc.] using the array index.


// data for the first 'reality' section page.
reality_pages[1] = '';

// ----------------------------------------------------------------------------
// data for the second 'reality' section page.

// - 1. modal window items.

reality_page_two_item_artist_name_one = 'IKEUCHI Hiroto';
reality_page_two_item_artist_url_one = 'https://mobile.twitter.com/kthomas82468237/status/91234973932827033';
reality_page_two_item_url_one = 'https://www.teenvogue.com/story/fbi-webcam-surveillance-meme-and-truth';
reality_page_two_item_title_one = `Cover your @#%!$# webcam, already`;
reality_page_two_item_text_one = `
		    <p>
 		      Should you cover your webcam? It's a smart privacy and security move.
		    </p>
		    <p>
 		      If your webcam is really hacked, it's not likely to be a benevolent and bored FBI agent watching. 
		    </p>
 		    <p>
  		      Taping over your webcam is one way to keep your laptop or phone safe, but <a href="https://twitter.com/tarah">Tarah Wheeler</a> also recommends the usual security basics: enable two-factor authentication, run automatic updates, turn on a firewall, backup your data in case you need to wipe your device clean, never click links from strangers, and use strong passwords or a password manager.
    `;

reality_page_two_item_artist_name_two = 'Artist Two';
reality_page_two_item_artist_url_two = 'https://artist-two';
reality_page_two_item_url_two = 'https://spectrum.ieee.org/view-from-the-valley/computing/networks/san-diegos-smart-streetlight-network-yielding-a-firehose-of-data';
reality_page_two_item_title_two = `San Diego’s smart streetlights yield a firehose of data`;
reality_page_two_item_text_two = `
		    <p>
		      2,000 sensor-laden streelights have begun gathering pictures, sounds, and other data about vehicles, traffic -- and people.
		    </p>
		    <p>
		      More useful to city planners than parking will be what Caldwell calls “mobility” data—information on how people and vehicles move through the city.
		    </p>
    `;

reality_page_two_item_artist_name_three = 'Artist Three';
reality_page_two_item_artist_url_three = 'https://artist-three';
reality_page_two_item_url_three = 'https://www.mercurynews.com/2019/01/21/it-was-five-minutes-of-sheer-terror-hackers-infiltrate-east-bay-familys-nest-surveillance-camera-send-warning-of-incoming-north-korea-missile-attack/';
reality_page_two_item_title_three = `5 minutes of sheer terror`;
reality_page_two_item_text_three = `
		    <p>
		      Hackers infiltrate East Bay family’s Nest surveillance camera, send warning of incoming North Korea missile attack.
		    <p>
		    <p>
		      The fabricated ICBM missile warning over the home's Nest system sent the East Bay family into panic.
		    </p>
    `;

reality_page_two_item_artist_name_four = 'Artist Four';
reality_page_two_item_artist_url_four = 'https://artist-four';
reality_page_two_item_url_four = 'https://url-four';
reality_page_two_item_title_four = `Item four title`;
reality_page_two_item_text_four = `
		    <p>
		     Item four text. 
		    </p>
    `;

reality_page_two_item_artist_name_five = 'Artist Five';
reality_page_two_item_artist_url_five = 'https://artist-five';
reality_page_two_item_url_five = 'https://url-five';
reality_page_two_item_title_five = `Item five title`;
reality_page_two_item_text_five = `
		    <p>
		     Item five text. 
		    </p>
    `;


reality_page_two_item_artist_name_six = 'Artist six';
reality_page_two_item_artist_url_six = 'https://artist-six';
reality_page_two_item_url_six = 'https://www.buzzfeednews.com/article/charliewarzel/you-have-zero-privacy-anyway-get-over-it';
reality_page_two_item_title_six = `When is social-media "connection" a euphemism for privacy invasion?`;
reality_page_two_item_text_six = `
		    <p>
		      Many of the most troubling privacy overreaches happen largely out of sight.
		    </p>
		    <p>
		      Our information is being mined and exploited, but it’s not always clear to what extent.
		    </p>
    `;

reality_page_two_item_artist_name_seven = 'Artist Seven';
reality_page_two_item_artist_url_seven = 'https://artist-seven';
reality_page_two_item_url_seven =  'https://www.nbcnews.com/health/health-news/long-acting-contraceptive-patch-gives-women-diy-option-birth-control-n958646';
reality_page_two_item_title_seven = `DIY Female microneedle contraceptive patch`;
reality_page_two_item_text_seven = `
		    <p>
		      Microneedle contraceptive patches offer women DIY birth control.
		    </p>
		    <p>
		      Press the patch into your arm or leg and get a month’s worth of birth control — no doctor visit needed.
		      Each dose would cost just $1, primarily intended for women in developing countries.
		    </p>
    `;

reality_page_two_item_artist_name_eight = 'Artist Eight';
reality_page_two_item_artist_url_eight = 'https://artist-eight';
reality_page_two_item_url_eight =  'https://www.nbcnews.com/health/health-news/long-acting-contraceptive-patch-gives-women-diy-option-birth-control-n958646';
reality_page_two_item_title_eight = `Item eight title`;
reality_page_two_item_text_eight = `
		    <p>
		      Item eight text.
		    </p>
    `;

// ---
// - 2. page items.

reality_page_two_item_page_text_six = `
		     <p>
		       Is your phone spying on you?
		     </p>
		     <p>
		       Smartphones, social media, and email are just the start.
		       Firms invest heavily in gathering user data and do so in a number of clever ways.
		     </p>
		     <p>
		       But it gets far creepier. A massive ecosystem of advertisers and supporting companies
		       is dedicated to tracking your activity across the internet.
		     </p>
		     <p>
		       Perhaps even more alarmingly, this tracking does not stop at online data.
		       Tech firms are known to purchase data from financial organisations
		       about user purchases in the real world to supplement their ad offerings.
		     </p>
`;

// ---

reality_pages[2] = {
    'gallery_image_urls': ['images/reality_images/DKlRPRyUEAAZF3F.jpeg',
			   'images/reality_images/james-paick-building-painting-01-copy.jpg',
			   'images/reality_images/biomechanic.jpg',
			   'images/reality_images/liudmila-kirdiashkina-liu-k-hawksuit2.jpg',
			   'images/reality_images/sergey-zabelin-yakuza-1200.jpg',
			   'images/reality_images/df836c2c2f3fd6e09d5b5687_rw_600.jpg',
			   'images/reality_images/jacek-babinski-c.jpg',
			   'images/reality_images/wojciech-wilk-fem01.jpg',
			  ],

    'modal_body_items': [{'title': reality_page_two_item_title_one,
			  'text': reality_page_two_item_text_one,
			  'url': reality_page_two_item_url_one,
			 },
			 {'title': reality_page_two_item_title_two,
			  'text': reality_page_two_item_text_two,
			  'url': reality_page_two_item_url_two,
			 },
			 {'title': reality_page_two_item_title_three,
			  'text': reality_page_two_item_text_three,
			  'url': reality_page_two_item_url_three,
			 },
			 {'title': reality_page_two_item_title_four,
			  'text': reality_page_two_item_text_four,
			  'url': reality_page_two_item_url_four,
			 },
			 {'title': reality_page_two_item_title_five,
			  'text': reality_page_two_item_text_five,
			  'url': reality_page_two_item_url_five,
			 },
			 {'title': reality_page_two_item_title_six,
			  'text': reality_page_two_item_text_six,
			  'url': reality_page_two_item_url_six,
			 },
			 {'title': reality_page_two_item_title_seven,
			  'text': reality_page_two_item_text_seven,
			  'url': reality_page_two_item_url_seven,
			 },
			 {'title': reality_page_two_item_title_eight,
			  'text': reality_page_two_item_text_eight,
			  'url': reality_page_two_item_url_eight,
			 },
			],
    
    'page_body_items': [{'title': reality_page_two_item_title_one,
			  'text': reality_page_two_item_text_one,
			  'url': reality_page_two_item_url_one,

			  'artist_name': reality_page_two_item_artist_name_one,
			  'artist_url': reality_page_two_item_artist_url_one,
			 },
			 {'title': reality_page_two_item_title_two,
			  'text': reality_page_two_item_text_two,
			  'url': reality_page_two_item_url_two,

			  'artist_name': reality_page_two_item_artist_name_two,
			  'artist_url': reality_page_two_item_artist_url_two,
			 },
			 {'title': reality_page_two_item_title_three,
			  'text': reality_page_two_item_text_three,
			  'url': reality_page_two_item_url_three,

			  'artist_name': reality_page_two_item_artist_name_three,
			  'artist_url': reality_page_two_item_artist_url_three,
			 },
			 {'title': reality_page_two_item_title_four,
			  'text': reality_page_two_item_text_four,
			  'url': reality_page_two_item_url_four,

			  'artist_name': reality_page_two_item_artist_name_four,
			  'artist_url': reality_page_two_item_artist_url_four,
			 },
			 {'title': reality_page_two_item_title_five,
			  'text': reality_page_two_item_text_five,
			  'url': reality_page_two_item_url_five,

			  'artist_name': reality_page_two_item_artist_name_five,
			  'artist_url': reality_page_two_item_artist_url_five,
			 },
			 {'title': reality_page_two_item_title_six,
			  'text': reality_page_two_item_page_text_six,
			  'url': reality_page_two_item_url_six,

			  'artist_name': reality_page_two_item_artist_name_six,
			  'artist_url': reality_page_two_item_artist_url_six,
			 },
			 {'title': reality_page_two_item_title_seven,
			  'text': reality_page_two_item_text_seven,
			  'url': reality_page_two_item_url_seven,

			  'artist_name': reality_page_two_item_artist_name_seven,
			  'artist_url': reality_page_two_item_artist_url_seven,
			 },
			 {'title': reality_page_two_item_title_eight,
			  'text': reality_page_two_item_text_eight,
			  'url': reality_page_two_item_url_eight,

			  'artist_name': reality_page_two_item_artist_name_eight,
			  'artist_url': reality_page_two_item_artist_url_eight,
			 },
			],

}


//-->

<!--

// define the zine section interface elements.
zine_section_names = [{key:'feature',
		       nav_id:'zine_modal_content_menu_feature_option',
		       text_id:'modal_feature_text_container',
		       gallery_id: 'modal_feature_gallery_container',
		       label:'Feature'},
		      {key:'reality',
		       nav_id:'zine_modal_content_menu_reality_option',
		       text_id:'modal_reality_text_container',
       		       gallery_id: 'modal_reality_gallery_container',
		       label: 'Reality'},
		      {key:'dreams',
		       nav_id:'zine_modal_content_menu_dreams_option',
		       text_id:'modal_dreams_text_container',
       		       gallery_id: 'modal_dreams_gallery_container',
		       label: 'Dreams'},
		     ];

// ---

function insert_zine_data(page_number)
{
 insert_modal_window_data(page_number);
 insert_page_window_data(page_number);
}

function insert_page_window_data(page_number)
{
 // insert 'reality' text items.
 insert_page_items('reality', page_number);

 // insert 'dreams' text items.
 insert_page_items('dreams', page_number);
}

function insert_page_items(item_type, page_number)
{
 // retrieve the number of text items for this page.
 switch (item_type)
 {
  case 'reality':
     page_text_items = reality_pages[page_number].page_body_items;
     page_item_images = reality_pages[page_number].gallery_image_urls;
     number_of_text_items = page_text_items.length;
  break;

  case 'dreams':
     page_text_items = dreams_pages[page_number].page_body_items;
     page_item_images = dreams_pages[page_number].gallery_image_urls;
     number_of_text_items = modal_text_items.length;
  break;
 }

 for (i = 0; i < number_of_text_items; i++)
 {
  item_id_number = i + 1;
  item_id = "zine_text_" + item_type + "_section_" + item_id_number;

  page_text_item_obj = document.getElementById(item_id);
  // insert the background image.
  // page_text_item_obj.getElementsByClassName('zine_inner_text_body')[0].style.backgroundImage = "url('" + page_item_images[i] + "')";
  page_text_item_obj.style.backgroundImage = "url('" + page_item_images[i] + "')";
  // insert the item heading.
  page_text_item_obj.getElementsByClassName('zine_text_item_heading')[0].innerHTML = page_text_items[i].title;
  // insert the item text.
  page_text_item_obj.getElementsByClassName('zine_text_item_body')[0].innerHTML = page_text_items[i].text;
     
  // ---

  // insert the 'read more' URL [if any].
  try
  {
   // inject the item URL into...
   // ...the main pane item's "read more" link.
   page_text_item_obj.getElementsByClassName('zine_text_item_read_full_story_link')[0].href = page_text_items[i].url;
   right_pane_item_id = item_type + "_item_" + item_id_number;
   // ...the gallery item's "read more" link.
   right_pane_item_obj = document.getElementById(right_pane_item_id);
   right_pane_item_obj.getElementsByClassName('read_full_story_link')[0].href = page_text_items[i].url;
  } catch(e){}

  // --- insert the artist citation [if any].
  try
  {
   artist_citation_obj = page_text_item_obj.getElementsByClassName('artist_citation_container')[0];
   artist_citation_link_obj = artist_citation_obj.getElementsByClassName('artist_citation_link')[0];
   //    - inject the artist URL.
   artist_citation_link_obj.href = page_text_items[i].artist_url;
   //    - inject the artist name.
   artist_citation_link_obj.innerHTML = page_text_items[i].artist_name;
  } catch(e){}
     
 }
}




function insert_modal_window_data(page_number)
{
 // insert the modal feature item title.
 item_id = 'modal_feature_title';
 insert_text_data(item_id, modal_feature_title);

 // insert the modal feature body data.
 item_id = 'modal_feature_body';
 insert_text_data(item_id, modal_feature_body);

 // ---

 // insert the modal feature main image URL.
 item_id = 'modal_window_page_left_pane_main_image';
 insert_background_image(item_id, modal_feature_image_urls[0]);

 // insert modal feature image gallery thumbnail image URLs.
 insert_modal_image_gallery('feature');

 // insert modal 'reality' image gallery thumbnail image URLs.
 insert_modal_image_gallery('reality', page_number);
 insert_modal_text_items('reality', page_number);

 // insert modal 'dreams' image gallery thumbnail image URLs.
 insert_modal_image_gallery('dreams', page_number);
 insert_modal_text_items('dreams', page_number);
}

function insert_text_data(item_id, item_data)
{
 document.getElementById(item_id).innerHTML = item_data;
}

function insert_background_image(item_id, item_data)
{
 document.getElementById(item_id).style = "background-image:url('" + item_data + "')";
}


function insert_modal_image_gallery(item_type, page_number)
{
 switch (item_type)
 {
  case 'feature':
     modal_gallery_urls = modal_feature_image_urls;
     number_of_images = modal_gallery_urls.length;
  break;

  case 'reality':
     modal_gallery_urls = reality_pages[page_number].gallery_image_urls;
     number_of_images = modal_gallery_urls.length;
  break;

  case 'dreams':
     modal_gallery_urls = dreams_pages[page_number].gallery_image_urls;
     number_of_images = modal_gallery_urls.length;
  break;
 }

 for (i = 0; i < number_of_images; i++)
 {
  item_id_number = i + 1;
  item_id = item_type + '_item_' +  item_id_number;
  insert_background_image(item_id, modal_gallery_urls[i]); 
 }
}

function insert_modal_text_items(item_type, page_number)
{
 // retrieve the number of text items for this page.
 switch (item_type)
 {
  case 'reality':
     modal_text_items = reality_pages[page_number].modal_body_items;
     number_of_text_items = modal_text_items.length;
  break;

  case 'dreams':
     modal_text_items = dreams_pages[page_number].modal_body_items;
     number_of_text_items = modal_text_items.length;
  break;
 }

 for (i = 0; i < number_of_text_items; i++)
 {
  item_id_number = i + 1;
  item_id = item_type + '_item_' +  item_id_number + '_text';
  modal_text_item_obj = document.getElementById(item_id);
  // insert the item heading.
  modal_text_item_obj.getElementsByClassName('modal_item_heading')[0].innerHTML = modal_text_items[i].title;
  // insert the item text.
  modal_text_item_obj.getElementsByClassName('modal_item_text')[0].innerHTML = modal_text_items[i].text;
  // insert the 'read more' URL [if any].
  try
  {
   modal_text_item_obj.getElementsByClassName('read_full_story_link')[0].href = modal_text_items[i].url;
  } catch(e){}
 }
}


// ---



function illuminate_page_item_background(this_obj)
{
 this_obj.getElementsByClassName('zine_inner_text_body')[0].style.backgroundColor = 'rgba(0,0,0, .3)';
}

function dim_page_item_background(this_obj)
{
 this_obj.getElementsByClassName('zine_inner_text_body')[0].style.backgroundColor = 'rgba(0,0,0, .6)';
}



function illuminate_background_image(parent_obj)
{
 parent_obj.parentNode.getElementsByClassName('zine_text_item_section_background')[0].style.opacity=1;
}

function dim_background_image(parent_obj)
{
 parent_obj.parentNode.getElementsByClassName('zine_text_item_section_background')[0].style.opacity=0.4;
}

function get_spatterdots(number_of_spatter_characters)
{
 spatter_character_listing = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
 spatter_characters = Array.apply(null, Array(number_of_spatter_characters)).map(function() { return spatter_character_listing.charAt(Math.floor(Math.random() * spatter_character_listing.length)); }).join('');

 return spatter_characters;
}

function load_spatterdots()
{
 number_of_spatterdots = 3;
 number_of_spatterdot_headings = 4;

 for (i = 0; i < number_of_spatterdot_headings; i++)
 { document.getElementsByClassName('spatter_text_heading')[i].innerHTML = get_spatterdots(number_of_spatterdots); }
}

function deselect_all_nav_items(zine_page_nav_items)
{
 // retrieve collection of nav items.
 zine_page_nav_items = document.getElementById('zine_page_nav').getElementsByTagName('li');

 // for each nav item,
 for (i = 0; i < zine_page_nav_items.length; i++)
 {
  // reset background color.
  zine_page_nav_items[i].style.background = 'rgba(0,0,0, .3)';
 }
}

function select_nav_item(this_obj)
{
 // deselect all nav items.
 deselect_all_nav_items();

 // for selected nav item, set background color.
 this_obj.style.background = 'rgba(204,102,204, .3)';
}

function deselect_nav_item(this_obj)
{
 // deselect all nav items.
 deselect_all_nav_items();

 // return selection to the page's selected nav item.
 // rem: the page's selected nav item index is set in a <script...> tag on the page itself], as of 2019.03.03.
 selected_page_nav_item_obj = document.getElementById('zine_page_nav').getElementsByTagName('li');
 selected_page_nav_item_obj[selected_page_nav_index].style.background = 'rgba(204,102,204, .3)';
}

function get_background_image_url(this_obj)
{
 this_background_image_url = this_obj.style.backgroundImage.slice(4, -1).replace(/["']/g, "");
 return this_background_image_url;
}

function display_gallery_image(this_obj)
{
 // extract the background image URL from the moused-over DOM object.
 // credit: https://stackoverflow.com/a/32789556
 this_background_image_url = get_background_image_url(this_obj);

 // insert the image into the main image background div.
 document.getElementById('modal_window_page_left_pane_main_image').style.backgroundImage = "url('" + this_background_image_url + "')";

 // ---

 // reset all thumbnail image highlights.
 /* thumbnail_item_collection = this_obj.parentNode.getElementsByClassName('right_pane_gallery_item');
 for (i = 0; i < thumbnail_item_collection.length; i++)
 {
  this_thumbnail_obj = thumbnail_item_collection[i];
  this_thumbnail_obj.style.border = "1px solid #333";
 } */

 // de-select the previously-viewed image thumbnail [if any].
 // deselect_previously_viewed_image_thumbnail();

 // highlight the selected image.
 this_obj.style.border = "1px solid #CCC";
}

function hide_all_text_sections_for_item_type(item_type)
{
 // hide all gallery text items of this type.
 this_modal_gallery_items_container_id = "modal_" + item_type + "_text_container";
 this_modal_gallery_items_container_obj = document.getElementById(this_modal_gallery_items_container_id);

 this_modal_gallery_items_collection = this_modal_gallery_items_container_obj.getElementsByClassName('modal_gallery_text_item');
 for (i = 0; i < this_modal_gallery_items_collection.length; i++)
 {
  this_text_item_obj = this_modal_gallery_items_collection[i];
  this_text_item_obj.style.display = "none";
 }
}

function hide_previously_viewed_text_item()
{
  // instantiate the 'previous item' type and number DOM objects.
  previous_modal_item_type_obj = document.getElementById('previous_modal_item_type');
  previous_modal_item_number_obj = document.getElementById('previous_modal_item_number');

  // continnue function execution only if this is _not_ a 'feature' item.
  previous_modal_item_type = previous_modal_item_type_obj.value;
  if (previous_modal_item_type == 'feature'){ return; }

  // ---

  // if there is record of a previously-viewed item,
  previous_modal_item_number = previous_modal_item_number_obj.value;
  if ((previous_modal_item_type != '') && (previous_modal_item_number != ''))
  {
   // - hide it.
   previous_text_item_display_id = previous_modal_item_type + "_item_" + previous_modal_item_number + "_text";
   document.getElementById(previous_text_item_display_id).style.display = 'none';
  }
}

function deselect_previously_viewed_image_thumbnail()
{
 // instantiate the 'previous item' type and number DOM objects.
 previous_modal_item_type_obj = document.getElementById('previous_modal_item_type');
 previous_modal_item_number_obj = document.getElementById('previous_modal_item_number');

 // if there is record of a previously-viewed item,
 previous_modal_item_type = previous_modal_item_type_obj.value;
 previous_modal_item_number = previous_modal_item_number_obj.value;
 if ((previous_modal_item_type != '') && (previous_modal_item_number != ''))
 {
  // - deselect its thumbnail image.
  previous_image_thumbnail_item_id = previous_modal_item_type + "_item_" + previous_modal_item_number;
  document.getElementById(previous_image_thumbnail_item_id).style.border = "1px solid #333";

 // - hide the 'read more' link section.
 hide_gallery_read_more_section(previous_modal_item_type, previous_modal_item_number);
 }
}

function set_previously_viewed_text_item_record(item_type, item_number)
{
 // set the 'previously-viewed item' record.
 previous_modal_item_type_obj = document.getElementById('previous_modal_item_type');
 previous_modal_item_number_obj = document.getElementById('previous_modal_item_number');

 previous_modal_item_type_obj.value = item_type;
 previous_modal_item_number_obj.value = item_number;
}

function display_zine_modal_gallery_item(item_type, item_state, this_obj)
{
 // retrieve the current 'hover effects' boolean value.
 hover_effects_obj = document.getElementById('allow_modal_image_gallery_thumbnail_hover_effects');
 allow_modal_image_gallery_thumbnail_hover_effects = hover_effects_obj.value;

 // if the user has clicked on the page grid [not within the modal window itself],
 // set the 'do not allow hover effects' record to 0 [i.e. 'no don allow hover effects'].
 if (item_state == 'no_hover'){ hover_effects_obj.value = 0; }

 // f the user has clicked a gallery item in the modal window,
 if ((item_state == 'hover') && (allow_modal_image_gallery_thumbnail_hover_effects == 0))
 {
  // enable hover effects.
  hover_effects_obj.value = 1;
  // exit the function.
  return;
 }

 // ---

 // de-select the previously-viewed image thumbnail [if any].
 deselect_previously_viewed_image_thumbnail();

 // hide the previously-viewed text item [if any].
 hide_previously_viewed_text_item();

 // ---

 // retrieve the item number for the currently-viewed gallery item.
 item_number = document.getElementById('modal_item_number').value;

 if ((item_type == 'reality') || (item_type == 'dreams'))
 {
  // hide all gallery text items of this type.
  // hide_all_text_sections_for_item_type(item_type);

  // if the item is a 'reality' item,
  if (item_type == 'reality')
  {
   // display the 'read more' link section.
   show_gallery_read_more_section(item_type, item_number);
  }

  // ---
     
  this_modal_gallery_items_container_id = "modal_" + item_type + "_text_container";
  this_modal_gallery_items_container_obj = document.getElementById(this_modal_gallery_items_container_id);
    
  // display the selected text item.
  this_modal_gallery_items_container_obj.style.display = 'block';
  text_item_display_id = item_type + "_item_" + item_number + "_text";
  document.getElementById(text_item_display_id).style.display = 'block';

  // set the modal's "main pane" to a darker [more readable] background opacity.
  highlight_text_display_section(item_type);
 }

 // set the 'previously-viewed text item' record.
 set_previously_viewed_text_item_record(item_type, item_number);

 // display the selected image in the main pane.
 display_gallery_image(this_obj);

 // if the user has clicked an item thumbnail,
 // add a 'item has been clicked' record.
 if (item_state == 'click'){ document.getElementById('modal_item_state').value = 'click'; }
 else { document.getElementById('modal_item_state').value = ''; }
}

function highlight_text_display_section(item_type)
{
 // set the modal's "main pane" to a darker [more readable] background opacity.
 modal_text_container_id = "modal_" + item_type + "_text_container";
 modal_text_container_obj = document.getElementById(modal_text_container_id);

 modal_text_container_obj.style.background = 'rgba(0,0,0, .6)';
    
 // ---

 text_container_obj = document.getElementById('modal_window_page_left_pane_main_text_container');

 // if this is a 'dreams' item;
 if (item_type == 'dreams')
 {
  // get the height of the main text pane, and resize as desired.
  main_pane_height_px = (document.getElementById('modal_window_page_left_pane').offsetHeight / 2) + "px";
  // get the height of the text container.
  text_container_obj.style.height = main_pane_height_px;
  // remove the 'shroud' background colour/opacity.
  text_container_obj.style.background = 'none';
  // add a backrgound to the modal text item.
  modal_text_container_obj.style.background = 'rgba(0,0,0, .1)';
  // remove the border.
  text_container_obj.style.border = 'none';
  // add a border to the modal text item.
  modal_text_container_obj.style.border = '1px solid rgba(0,0,0, .1)';
 }
 else
 {
  resize_modal_item_box(item_type);
 }

 // if this is _not_ the 'feature' item,
 if (item_type != 'feature')
 {
  // remove the outer container div left padding.
  text_container_obj.style.paddingLeft = '0';
  // create the inner container div left padding.
  modal_text_container_obj.style.paddingLeft = '.2em';
  // create the modal item background color/opacity.
  modal_text_container_obj.style.background = 'rgba(0,0,0, .3)';
 }
}

function revert_text_display_section(item_type)
{
 // if the user has clicked an item,
 // exit the function [i.e. don't do anything here.]
 item_state = document.getElementById('modal_item_state').value;
 if (item_state == 'click'){ return };
    
 // ---

 modal_text_container_id = "modal_" + item_type + "_text_container";
 modal_text_container_obj = document.getElementById(modal_text_container_id);

 // get the height of the main text pane, and resize as desired.
 modal_text_container_height_px = modal_text_container_obj.height + "px";

 // ---

 // if this is _not_ a 'dreams' item;
 if (item_type != 'dreams')
 {
  // set the modal box height.
  text_container_obj = document.getElementById('modal_window_page_left_pane_main_text_container');
  text_container_obj.style.height = modal_text_container_height_px;

  // create the modal item background color/opacity.
  text_container_obj.style.background = 'rgba(0,0,0, .3)';
  text_container_obj.style.border = '1pi solid #000';
 }
  // if this is a 'reality' item;
 if (item_type == 'reality')
 {
  // retrieve the item number for the currently-viewed gallery item.
  item_number = document.getElementById('modal_item_number').value;

  // hide the 'read more' thumbnail link section.
  hide_gallery_read_more_section(item_type, item_number);
 }
    
 // set the modal's "main pane" to a lighter [more transparent] background opacity.
 modal_text_container_obj.style.background = 'none';
}

function show_gallery_read_more_section(item_type, item_number)
{
 current_item_id = item_type + "_item_" + item_number;
 current_item_obj = document.getElementById(current_item_id);
 read_more_section_obj = current_item_obj.getElementsByClassName('right_pane_gallery_item_read_more_container')[0];
 read_more_section_obj.style.display = 'block';
}

function hide_gallery_read_more_section(item_type, item_number)
{
 if (item_type == 'reality')
 {
  current_item_id = item_type + "_item_" + item_number;
  current_item_obj = document.getElementById(current_item_id);
  read_more_section_obj = current_item_obj.getElementsByClassName('right_pane_gallery_item_read_more_container')[0];
  read_more_section_obj.style.display = 'none';
 }
}

function toggle_read_more_text_label(this_obj, toggle_state)
{
 switch (toggle_state)
 {
  case 'show':
     this_obj.getElementsByClassName('read_full_story_text')[0].style.display = 'block';
     break;
  case 'hide':
     this_obj.getElementsByClassName('read_full_story_text')[0].style.display = 'none';
     break;
 }
}

function open_zine_page_modal_window(item_type, item_number)
{
 // open the modal window.
 document.getElementById('zine_page_modal').style.display='block';

 // if the user has requested a certain item type,
 if (item_type != "")
 {
  // open the item's gallery
  display_zine_section(item_type);
 }

 // if the user has requested a certain item,
 if (item_number != "")
 {
  document.getElementById('modal_item_number').value = item_number;
  item_dom_id = item_type + "_item_" + item_number;
  image_obj = document.getElementById(item_dom_id);
  display_zine_modal_gallery_item(item_type, 'no_hover', image_obj);
 }

    
 // ---

 // deselect the first 'feature' gallery image.
 document.getElementById('page_text_feature_image').style.opacity = .4;

 // deselect the 'feature' gallery thumbnail images.
 deselect_all_page_feature_image_thumbnail_items();
}

function allow_image_gallery_hover_effects_for_modal_background_hover()
{
 document.getElementById('allow_modal_image_gallery_thumbnail_hover_effects').value = 1;
 // rem: setting the 'hover effects' value to '1' [i.e. "on"] means that when the user
 //      hovers over a gallery item, the item will display its hover effect.
 //      This is needed in order to 'unlock' the hover effect in case the user is
 //      opening the modal, and the mouse pointer is _not_ over an image thumbnail
 //      [we only want to 'lock' so that the hover effect isn't accidentally triggered
 //      when the user opens the modal window after selecting a different image
 //      from the underlying page image thumbnail grid].

 // deselect the first 'feature' gallery image.
 document.getElementById('page_text_feature_image').style.opacity = .4;

 // deselect the 'feature' gallery thumbnail images.
 deselect_all_page_feature_image_thumbnail_items();
}

function close_zine_page_modal_window()
{
 // close the modal window.
 document.getElementById('zine_page_modal').style.display='none';

 // reset the modal window statusline.
 reset_modal_window_statusline();

 // disallow hover effects for modal window items.
 document.getElementById('allow_modal_image_gallery_thumbnail_hover_effects').value = 0;
    
 // highlight the first 'feature' gallery image.
 document.getElementById('page_text_feature_image').style.opacity = 1;

 // highlight the first 'feature' gallery thumbnail image.
 deselect_all_page_feature_image_thumbnail_items();
 feature_thumbnail_gallery_obj = document.getElementById('page_text_feature_image_gallery_thumbnails_section');
 first_feature_thumbnail_item_obj = feature_thumbnail_gallery_obj.getElementsByTagName('li')[0];
 first_feature_img_obj = first_feature_thumbnail_item_obj.getElementsByTagName('img')[0];
 first_feature_img_obj.style.opacity = 1;
 // inject the first image URL into the 'feature image' img tag.
 document.getElementById('page_text_feature_image').src = first_feature_img_obj.src;
}

function display_zine_section(zine_section_name)
{
 // display the desired section's text data.
 display_zine_text_section(zine_section_name);
 // display the desired section's gallery data.
 display_zine_gallery_section(zine_section_name);
 // inject the first gallery item into the main window.
 inject_first_image_into_main_window(zine_section_name);
 // update menu option display.
 display_zine_nav_section(zine_section_name);
 // set the 'selected modal nav menu' item.
 document.getElementById('modal_menu_selected_option').value = zine_section_name;
}

function toggle_modal_content_menu_selection(zine_section_name, toggle_state)
{
 // deselect all menu items.
 for (i = 0; i < zine_section_names.length; i++)
 {
  selected_nav_item_obj = document.getElementById(zine_section_names[i].nav_id);
  selected_nav_item_obj.className = 'zine_modal_content_menu_selected_item';

  if ((zine_section_names[i].key == zine_section_name) && (toggle_state == 'mouseover'))
  {
   // highlight the selected zine section.
   document.getElementById(zine_section_names[i].nav_id).className = 'zine_modal_content_menu_selected_item';
   // update the modal nav section label.
   document.getElementById('modal_section_heading').innerHTML = zine_section_names[i].label;
  }
  else
  {
   // remove any highlight class from the zine section
   document.getElementById(zine_section_names[i].nav_id).className = '';
   // hide the selected item "selected star" element.
   selected_nav_item_obj.getElementsByClassName('selected_nav_item_star')[0].style.display = 'none';
  }
 }

 if (toggle_state == 'mouseout')
 {
  modal_menu_selected_option = document.getElementById('modal_menu_selected_option').value;
     
  // retrieve the modal mav selected nav id and label.
  for (i = 0; i < zine_section_names.length; i++)
  {
   if (zine_section_names[i].key == modal_menu_selected_option)
   {
    modal_menu_selected_nav_id = zine_section_names[i].nav_id;
    modal_menu_selected_label = zine_section_names[i].label;
   }
  }
  // highlight the selected zine section.
  document.getElementById(modal_menu_selected_nav_id).className = 'zine_modal_content_menu_selected_item';
  // update the modal nav section label.
  document.getElementById('modal_section_heading').innerHTML = modal_menu_selected_label;

  // display the selected item "selected star" element.
  modal_menu_selected_option_obj = document.getElementById(modal_menu_selected_nav_id);
  modal_menu_selected_option_obj.getElementsByClassName('selected_nav_item_star')[0].style.display = 'block';
 }
}

function display_zine_nav_section(zine_section_name)
{
 for (i = 0; i < zine_section_names.length; i++)
 {
  selected_nav_item_obj = document.getElementById(zine_section_names[i].nav_id);
  selected_nav_item_obj.className = 'zine_modal_content_menu_selected_item';

  if (zine_section_names[i].key == zine_section_name)
  { // highlight the selected zine section.
    document.getElementById(zine_section_names[i].nav_id).className = 'zine_modal_content_menu_selected_item';
    // display the selected item "selected star" element.
    selected_nav_item_obj.getElementsByClassName('selected_nav_item_star')[0].style.display = 'block';
    // update the modal nav section label.
    document.getElementById('modal_section_heading').innerHTML = zine_section_names[i].label;
  } 
  else
  {
   // remove any highlight class from the zine section
   document.getElementById(zine_section_names[i].nav_id).className = '';
   // hide the selected item "selected star" element.
   selected_nav_item_obj.getElementsByClassName('selected_nav_item_star')[0].style.display = 'none';
  }
 }
}

function display_zine_text_section(zine_section_name)
{
 // reset the 'modal item state' record.
 document.getElementById('modal_item_state').value = '';

 // de-select the previously-viewed image thumbnail [if any].
 deselect_previously_viewed_image_thumbnail();

 // hide the previously-viewed text item [if any].
 hide_previously_viewed_text_item();

 // ---

 // set the modal's "main pane" to a darker [more readable] background opacity.
 highlight_text_display_section(zine_section_name);

 for (i = 0; i < zine_section_names.length; i++)
 {
  if (zine_section_names[i].key == zine_section_name)
  { // display the selected zine section.
    document.getElementById(zine_section_names[i].text_id).style.display = 'block';

    /* if this is not the 'Feature' section [since the Feature section
       only has one item], */
    if (zine_section_name != 'feature')
    {
     // hide all gallery text items of this type.
     // hide_all_text_sections_for_item_type(zine_section_name);

     // display the first text item.
     text_item_display_id = zine_section_name + "_item_1_text";
     document.getElementById(text_item_display_id).style.display = 'block';

     // ---

     // set the modal's "main pane" to a darker [more readable] background opacity.
     modal_text_container_id = "modal_" + zine_section_name + "_text_container";
     modal_text_container_obj = document.getElementById(modal_text_container_id);

     background_opacity_setting = '.3';
     modal_text_container_obj.style.background = 'rgba(0,0,0, '+ background_opacity_setting + ')';

     if (zine_section_name != 'dreams')
     {
      // set the modal box height.
      text_container_obj = document.getElementById('modal_window_page_left_pane_main_text_container');
      text_container_obj.style.height = modal_text_container_height_px;

      // create the modal item background color/opacity.
      text_container_obj.style.background = 'rgba(0,0,0, .3)';
      text_container_obj.style.border = '1pi solid #000';
     }
    }
  }
  else
  { document.getElementById(zine_section_names[i].text_id).style.display = 'none'; }

  // set the 'previously-viewed text item' record.
  set_previously_viewed_text_item_record(zine_section_name, 1);
 }

 // resize the container box for this item.
 resize_modal_item_box(zine_section_name);
}

function resize_modal_item_box(zine_section_name)
{
 // if this is _not_ a 'dreams' section,
 if (zine_section_name != 'dreams')
 {
  // get the height of the main text pane, and resize as desired.
  modal_text_container_id = "modal_" + zine_section_name + "_text_container";
  // modal_text_container_height_px = document.getElementById(modal_text_container_id).offsetHeight + "px";
  // modal_text_container_height_px = document.getElementById(text_item_display_id).offsetHeight + "px";
  modal_text_container_height_px = document.getElementById(modal_text_container_id).offsetHeight + "px";
  text_container_obj = document.getElementById('modal_window_page_left_pane_main_text_container');
  text_container_obj.style.height = modal_text_container_height_px;
 }
}

function display_zine_gallery_section(zine_section_name)
{
 for (i = 0; i < zine_section_names.length; i++)
 {
  if (zine_section_names[i].key == zine_section_name)
  { // display the selected zine section.
    document.getElementById(zine_section_names[i].gallery_id).style.display = 'block';

    // ---

    modal_gallery_id = "modal_" + zine_section_name + "_gallery_container";
    this_obj = document.getElementById(modal_gallery_id);
    thumbnail_item_collection = this_obj.getElementsByClassName('right_pane_gallery_item');
    // reset all thumbnail image highlights for this section.
    /* for (j = 0; j < thumbnail_item_collection.length; j++)
    {
     this_thumbnail_obj = thumbnail_item_collection[j];
     this_thumbnail_obj.style.border = "1px solid #333";
    } */
      
    // de-select the previously-viewed image thumbnail [if any].
    // deselect_previously_viewed_image_thumbnail();

    // highlight the first image.
    thumbnail_item_collection[0].style.border = "1px solid #CCC";
  }
  else
  { document.getElementById(zine_section_names[i].gallery_id).style.display = 'none'; }
 }
}

function get_gallery_id(zine_section_name)
{
 //  for each zine section,
 for (i = 0; i < zine_section_names.length; i++)
 {
  // if this is the requested zine section,
  if (zine_section_names[i].key == zine_section_name)
  {
   // retrieve the gallery DOM ID.
   modal_gallery_id = zine_section_names[i].gallery_id;
   // exit the loop.
   break;
  }
 }

 return modal_gallery_id;
}

function inject_first_image_into_main_window(zine_section_name)
{
 // find the gallery ID.
 modal_gallery_id = get_gallery_id(zine_section_name);

 // test.
 // alert('modal_gallery_id: ' + modal_gallery_id);

 // retrieve first gallery item URL.
 this_gallery_obj = document.getElementById(modal_gallery_id);
 this_gallery_item_obj = this_gallery_obj.getElementsByClassName('right_pane_gallery_item')[0];
 first_background_image_url = get_background_image_url(this_gallery_item_obj);

 // test.
 // alert('first_background_image_url:' + first_background_image_url);

 document.getElementById('modal_window_page_left_pane_main_image').style.backgroundImage = "url('" + first_background_image_url + "')";
}

// ---

function page_setup(page_number, selected_section_name)
{
 insert_page_gallery_images(page_number);
 insert_zine_data(page_number);
 display_zine_section(selected_section_name);
}

// ---

function main(page_number, selected_section_name)
{
 // default to 'feature' section display if no section is specificed.
 if ((typeof selected_section_name == 'undefined') || (selected_section_name == '')){ selected_section_name = 'feature'; }

 // default to page 1 is no page is specificed.
 if ((typeof page_number == 'undefined') || (page_number == '')){ selected_section_name = 1; }

 // setup the page display.
 page_setup(page_number, selected_section_name);

 // when the page [DOM tree] has loaded, update the 'spatterdot' headings.
 document.onload = load_spatterdots();
}

function display_feature_image_on_page(this_obj)
{
 // inject the image URL into the 'feature image' img tag.
 document.getElementById('page_text_feature_image').src = this_obj.src;

 // illuminate the feature image.
 deselect_all_page_feature_image_thumbnail_items();
 this_obj.style.opacity = 1;
 document.getElementById('page_text_feature_image').style.opacity = 1;
}

function deselect_all_page_feature_image_thumbnail_items()
{
 feature_thumbnail_gallery_obj = document.getElementById('page_text_feature_image_gallery_thumbnails_section');
 feature_thumbnail_items_obj = feature_thumbnail_gallery_obj.getElementsByTagName('li');
 // deselect each feature image gallery thumbnail image.
 for (i = 0; i < feature_thumbnail_items_obj.length; i++)
 { feature_thumbnail_items_obj[i].getElementsByTagName('img')[0].style.opacity = .4; }
}

function display_link_url_in_modal_statusbar(this_obj, item_state)
{
 if (item_state == 'show')
 { // inject the item URL into the stastusline.
   item_url = this_obj.href;

   // instantiate the 'modal statusline' DOM object.
   modal_statusline_obj = document.getElementById('modal_statusline');

   // ---
   // remove the leading 'http[s]', 'www' and trailing slash, if any.
   item_url = item_url.replace(/^(http[s]?\:\/\/)/,"");
   item_url = item_url.replace(/^(www\.)/,"");
   item_url = item_url.replace(/\/$/, "");
   // ---

   // limit the URL to a maximum of [x] characters [in order to fit onscreen].
   left_substring_length = 45;
   right_substring_length = 25;
   left_url_substring = item_url.substr(0, left_substring_length);
   right_url_substring = item_url.substr(0, right_substring_length);

   // if the [non-truncated] URL is too long, add ellpsis after the shortened version.
   if (item_url.length > left_substring_length){ left_url_substring += "..."; }
   if (item_url.length > right_substring_length){ right_url_substring = "..." + right_url_substring; }

   modal_statusline_obj.getElementsByClassName('left_status_box')[0].innerHTML = left_url_substring;
   modal_statusline_obj.getElementsByClassName('right_status_box')[0].innerHTML = right_url_substring;
 }
 else
 { reset_modal_window_statusline(); }
}

function reset_modal_window_statusline()
{
 // instantiate the 'modal statusline' DOM object.
 modal_statusline_obj = document.getElementById('modal_statusline');

 default_statusline_message = 'starpunk.zone';

 modal_statusline_obj.getElementsByClassName('left_status_box')[0].innerHTML = default_statusline_message;
 modal_statusline_obj.getElementsByClassName('right_status_box')[0].innerHTML = default_statusline_message;
}

function insert_page_gallery_images(page_number)
{
 // insert 'reality' section images.
 reality_image_urls = reality_pages[page_number].gallery_image_urls;
 dreams_image_urls = dreams_pages[page_number].gallery_image_urls;

 page_gallery_urls = [{'type': 'reality',
		       'image_urls': reality_image_urls,},
		      {'type': 'dreams',
		       'image_urls': dreams_image_urls,},
		     ];

 // ---

 for (i = 0; i < page_gallery_urls.length; i++)
 {
  // retrieve the gallery type ['reality', 'dreams', etc.]
  gallery_image_type = page_gallery_urls[i]['type'];
  gallery_image_urls = page_gallery_urls[i]['image_urls'];
  for (j = 0; j < gallery_image_urls.length; j++)
  {
   image_number = j + 1;
   page_gallery_image_id = gallery_image_type + "_page_gallery_image_" + image_number;
   document.getElementById(page_gallery_image_id).style.backgroundImage = "url('" + gallery_image_urls[j] + "')";
  }
 }
}

//-->
